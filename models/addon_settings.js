"use strict";

module.exports = function (db, DataTypes) {
    var AddonSetting = db.define('AddonSetting', {
        clientKey: { type: DataTypes.STRING},
        key: { type: DataTypes.STRING },
        val: { type: DataTypes.STRING }
    }, {
        timestamps: false
    });

    AddonSetting.sync();

    return AddonSetting;
};
