"use strict";

module.exports = function (db, DataTypes) {
  var RoomService = db.define('RoomService', {
    clientKey: { type: DataTypes.STRING, allowNull: false },
    roomId: { type: DataTypes.STRING, allowNull: false },
    name: { type: DataTypes.STRING, allowNull: false },
    event: { type: DataTypes.STRING, allowNull: false },
    pattern: { type: DataTypes.STRING },
    reply: { type: DataTypes.TEXT, allowNull: false },
    webhook: { type: DataTypes.INTEGER },
    enable: { type: DataTypes.BOOLEAN, defaultValue: true },
    replyHtml: { type: DataTypes.BOOLEAN, defaultValue: false },
    notify: { type: DataTypes.BOOLEAN, defaultValue: false },
    owners: { type: DataTypes.JSON, defaultValue: {} },
    triggerCount: { type: DataTypes.INTEGER, defaultValue: 0 }
  });

  // create table
  // make sure that force is false on prod, unless you need to migrate...
  // or you could just write migrations shr
  RoomService.sync();

  return RoomService;
};
