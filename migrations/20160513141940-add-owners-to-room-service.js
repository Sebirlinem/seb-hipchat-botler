'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    queryInterface.addColumn(
        'RoomServices',
        'owners',
        {
          type: Sequelize.JSON,
          defaultValue: {}
        }
    );
  },

  down: function (queryInterface, Sequelize) {
    queryInterface.removeColumn('RoomServices', 'owners')
  }
};
