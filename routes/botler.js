var _ = require('lodash');
var ejs = require('ejs');

module.exports = function (app, addon) {

  var COMMON_EVENT_ATTRIBUTES = {
    roomName: {
      friendlyName: "Room Name",
      description: "Name of the room.",
      getter: function (payload) {
        return payload.item.room.name;
      }
    }
  };

  var EVENTS = {
    // TODO: add pattern matched term {{matched}} -> replace message for pattern
    room_message: {
      friendlyName: 'A new message in the room',
      attributes: _.extend({
        senderName: {
          friendlyName: "Sender Name",
          description: "The display user name.",
          getter: function (payload) {
            return payload.item.message.from.name;
          }
        },
        senderMentionName: {
          friendlyName: "Sender Mention Name",
          description: "User's @mention name.",
          getter: function (payload) {
            return '@' + payload.item.message.from.mention_name;
          }
        },
        message: {
          friendlyName: "Message",
          description: 'The message sent',
          getter: function (payload) { return payload.item.message.message }
        },
        mentions: {
          friendlyName: "Mentions",
          description: 'A list of mentioned users in this message.',
          getter: function (payload) { return _.map(payload.item.message.mentions, function(mention) {
          return '@' + mention['mention_name'];
        }).join(' ')} }
      }, COMMON_EVENT_ATTRIBUTES)
    },
    room_enter: {
      friendlyName: 'A user enters the room',
      attributes: _.extend({
        senderName: {
          friendlyName: "Sender Name",
          description: "The display user name.",
          getter: function (payload) {
            return payload.item.sender.name;
          }
        },
        senderMentionName: {
          friendlyName: "Sender Mention Name",
          description: "User's @mention name.",
          getter: function (payload) {
            return '@' + payload.item.sender.mention_name;
          }
        }
      }, COMMON_EVENT_ATTRIBUTES)
    },
    room_exit: {
      friendlyName: 'A user exits the room',
      attributes: _.extend({
        senderName: {
          friendlyName: "Sender Name",
          description: "The display user name.",
          getter: function (payload) {
            return payload.item.sender.name;
          }
        },
        senderMentionName: {
          friendlyName: "Sender Mention Name",
          description: "User's @mention name.",
          getter: function (payload) {
            return '@' + payload.item.sender.mention_name;
          }
        }
      }, COMMON_EVENT_ATTRIBUTES)
    },
    room_file_upload: {
      friendlyName: 'A user uploads a file to the room',
      attributes: _.extend({
        fileName: {
          friendlyName: "File Name",
          description: 'The name of the uploaded file',
          getter: function(payload) { return payload.item.file.name }
        },
        fileSize: {
          friendlyName: "File Size",
          description: 'The size of the file in bytes',
          getter: function(payload) { return payload.item.file.size }
        },
        fileUrl: {
          friendlyName: "File URL",
          description: 'The URL of the file',
          getter: function(payload) { return payload.item.file.url }
        },
        fileThumbnail: {
          friendlyName: "File Thumbnail",
          description: 'The URL of the thumbnail if it exists',
          getter: function(payload) { return payload.item.file.thumb_url }
        }
      }, COMMON_EVENT_ATTRIBUTES)
    },
    room_topic_change: {
      friendlyName: 'Someone changes the room topic',
      attributes: _.extend({
        topic: {
          friendlyName: "Topic",
          description: 'The new topic.',
          getter: function (payload) { return payload.item.topic }
        }
      }, COMMON_EVENT_ATTRIBUTES)
    }
  };

  addon.db = require('../models')(addon);

  app.get('/dialog', addon.authenticate(), function (req, res) {
    res.render('dialog', {
      events: EVENTS,
      identity: req.identity      
    });
  });

  app.get('/sidebar', addon.authenticate(), function (req, res) {    
    addon.db.RoomService.findAll({where: {clientKey: req.clientInfo.clientKey}, order: 'id DESC'})
    .then(function (roomServices) {

      var parsedRoomServices = _.map(roomServices, 'dataValues');      

      if (parsedRoomServices.length) {
        res.render('sidebar', {
          identity: req.identity,
          roomServices: parsedRoomServices,
          hipChatTheme: req.query.theme
        });
      } else {
        res.render('sidebar-empty', {
          identity: req.identity,
          hipChatTheme: req.query.theme
        });        
      }      
    });
  });

  app.get('/botler/attributes', function (req, res) {
    var event = req.query.event;
    try {
      var e = EVENTS[event];
      var html = require('../views/botler/attributes.ejs')({attributes: e.attributes})
      res.send(html);
    } catch (e) {
      addon.logger.error(e);
      res.sendStatus(418);
    }
  });

  app.post('/botler/callback', addon.authenticate(), function (req, res) {

    function removeOldWebhook(clientKey, whId) {

      addon.db.AddonSetting.findOne({where: {clientKey: clientKey, key: 'clientInfo'}}).then(function (clientInfo) {
        if (clientInfo) {
          var cInfo = JSON.parse(clientInfo.val);
          addon.hipchat.removeRoomWebhook(cInfo, cInfo.roomId, whId).then(function (data) {
            addon.logger.info("Deleted old webhook: ", whId);
          }).catch(function (err) {
            addon.logger.error(err);
          });
        }
      });
    }    

    function checkForMatchTerm(currentReply) {
      var indexOfSymbol = currentReply.indexOf('{{$');
      if (indexOfSymbol > -1) {
        var indexMatcher = currentReply.match(new RegExp('\{\{\\$(\\d+)\}\}'));
        if (indexMatcher) {
          var matchTermNum = parseInt(indexMatcher[1]);
          return matchTermNum;
        } else {
          return null;
        }            
      }
    }
    function checkForRand(currentReply) {
      var stringStart = "";
      var stringEnd = "";
      var indexOfRangeStart = currentReply.indexOf('[');
      var indexOfRangeEnd = currentReply.indexOf(']');
      if (indexOfRangeStart > -1) {
        //preserve the non-random bits
        if (indexOfRangeStart > 0) {
          stringStart = currentReply.split('[')[0];
        }
        if (indexOfRangeEnd < currentReply.length) {
          stringEnd = currentReply.split(']')[1];
        }
        //randomise the random bit
        var randRange = currentReply.split('[')[1];
        randRange = randRange.split(']')[0];
        randRange = randRange.split(',');
        var randGen = Math.floor(Math.random() * randRange.length);
        var randSelect = randRange[randGen];
        //string math it back together
        currentReply = "";
        if (stringStart != "") {
          currentReply = stringStart;
        }
        currentReply = currentReply + randSelect;
        if (stringEnd != "") {
          currentReply = currentReply + stringEnd;
          }
      }
      return currentReply;
    }
    
    var serviceId = req.query.id;
    var webhookId = req.body.webhook_id;
    addon.db.RoomService.findOne({where: {id: serviceId}}).then(function (service) {
      if (service) {
        res.sendStatus(200);

        if (service.webhook !== webhookId) {
          addon.logger.warn("Triggered does not match service webhook. Deleting webhook ", webhookId);
          removeOldWebhook(req.body.oauth_client_id, webhookId);
          return;
        }

        if (service.enable) {          
          var reply = service.reply;
          // parse reply attributes
          Object.keys(EVENTS[service.event].attributes).forEach(function (attributeKey) {
            var option = '{{' + attributeKey + '}}';
            if (reply.indexOf(option) > -1) {
              reply = reply.replace(option, EVENTS[service.event].attributes[attributeKey].getter(req.body));
            }
          });

          // match terms only if it's room_message event!
          if (service.event === 'room_message') {
            // handle matched terms
            var matchedTermIndex = checkForMatchTerm(reply);
            var matchedTerms = req.body.item.message.message.match(new RegExp(service.pattern));

            try {
              while (matchedTermIndex) {
                if (checkForMatchTerm(matchedTerms[matchedTermIndex])) {
                  break;
                }
                reply = reply.replace('{{$' + matchedTermIndex + '}}', matchedTerms[matchedTermIndex]);
                matchedTermIndex = checkForMatchTerm(reply);
              }
            } catch (e) {
              console.log(e);
            }
          }
          
          //handle random terms
		  reply = checkForRand(reply);

          // send the damn message
          addon.hipchat.sendMessage(req.clientInfo, service.roomId, reply, {options: {format: service.replyHtml ? 'html':'text', notify: service.notify, color: 'gray'}});

          //increment triggerCount
          service.triggerCount++;
          service.save();
        } else {
          addon.logger.info('Service is disabled. Not running.');
        }
      } else {
        addon.logger.warn("Could not find webhook callback for webhook with id: ", webhookId);
        removeOldWebhook(req.body.oauth_client_id, webhookId);
        res.sendStatus(418);
      }
    });

  });

  app.get('/botler/room_service', addon.authenticate(), function (req, res) {
    var serviceId = req.query.id;
    addon.db.RoomService.findOne({where: {id: serviceId}}).then(function (service) {
      if (service) {
        var plainService = service.get({plain: true});
        delete plainService['clientKey'];
        delete plainService['roomId'];        
        res.json(plainService);
      } else {
        res.sendStatus(500);
      }
    });
  });

  app.post('/botler/room_service', addon.authenticate(), function (req, res) {
    var newService = req.body,
    clientKey = req.clientInfo.clientKey,
    roomId = req.identity.roomId;        

    // add clientKey and roomId to the service object
    _.extend(newService, {
      clientKey: clientKey,
      roomId: roomId
    });

    // parse the owners array        
    newService.owners = JSON.parse(newService.owners);

    addon.db.RoomService.create(newService).then(function(service) {
      if (service) {
        // register the webhook
        addon.hipchat.addRoomWebhook(clientKey, roomId, {
          url: addon.descriptor.links.homepage + '/botler/callback?id=' + service.id,
          pattern: service.pattern,
          event: service.event,
          authentication: "jwt",
          name: service.name
        }).then(function (webhook) {
          addon.logger.info('Webhook successfully created: ', webhook.body);
          service.update({webhook: webhook.body.id});
          res.sendStatus(200);
        });
      }
      else {
        res.sendStatus(500);
      }
    });
  });

  app.put('/botler/room_service', addon.authenticate(), function (req, res) {
    var reqService = req.body;
    addon.db.RoomService.findOne({where: {id: reqService.id}}).then(function (service) {
      if (service) {
        addon.logger.info('Updating room service with id: ', reqService.id)
        delete reqService['id'];
        if (reqService.event !== 'room_message') {
          reqService.pattern = '';
        }

        var oldEvent = service.event
        var oldPattern = service.pattern;

        reqService.owners = service.owners;
        var newOwner = JSON.parse(reqService.newOwner);
        delete reqService.newOwner;

        if (!service.owners.hasOwnProperty(newOwner.id)) {
          reqService.owners[newOwner.id] = {name: newOwner.name, mentionName: newOwner.mentionName};
        }

        service.update(reqService).then(function(data) {
          if (reqService.event !== oldEvent || (reqService.event === oldEvent && oldEvent === 'room_message' && reqService.pattern !== oldPattern)) {
            addon.logger.info('Webhook event is different. Replacing existing webhook...');
            addon.logger.info("Deleting webhook: ", service.webhook);
            addon.hipchat.removeRoomWebhook(req.clientInfo, service.roomId, service.webhook).then(function(d){
              addon.logger.info('Webhook successfully deleted.');
              addon.hipchat.addRoomWebhook(req.clientInfo, service.roomId, {
                url: addon.descriptor.links.homepage + '/botler/callback?id=' + service.id,
                pattern: reqService.pattern,
                event: reqService.event,
                authentication: "jwt",
                name: reqService.name
              }).then(function(webhook){
                addon.logger.info('Webhook successfully created: ', webhook.body);
                service.update({webhook: webhook.body.id});
                res.sendStatus(200);
              }).catch(function(err) {
                addon.logger.error(err);
                res.sendStatus(500);
              });
            }).catch(function(err) {
              addon.logger.error(err);
              res.sendStatus(500);
            });
          } else {
            res.sendStatus(200);
          }
        });
      } else {
        res.sendStatus(418);
      }
    });
  });

  app.delete('/botler/room_service', addon.authenticate(), function (req, res) {
    var serviceId = req.body.id;
    addon.db.RoomService.findOne({where: {id: serviceId}}).then(function (service) {
      if (service) {
        addon.logger.info("Deleting webhook: ", service.webhook);
        addon.hipchat.removeRoomWebhook(req.clientInfo, service.roomId, service.webhook).then(function (data) {
          service.destroy();
          res.sendStatus(200);
        }).catch(function (err) {
          res.sendStatus(500);
        });
      } else {
        res.sendStatus(418);
      }
    });
  });
}
